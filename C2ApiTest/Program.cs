﻿using Concept2;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Erg3
{
    class Program
    {
        private PerformanceMonitor pm;

        static void Main(string[] args)
        {
            PMUSBInterface.Initialize();
            PMUSBInterface.InitializeProtocol(999);

            ushort numErgs = PMUSBInterface.DiscoverPMs(PMUSBInterface.PMtype.PM3TESTER_PRODUCT_NAME);
            Console.WriteLine("Found {0} PM3TESTER_PRODUCT_NAME...", numErgs);
            numErgs = PMUSBInterface.DiscoverPMs(PMUSBInterface.PMtype.PM3_PRODUCT_NAME);
            Console.WriteLine("Found {0} PM3_PRODUCT_NAME...", numErgs);
            numErgs = PMUSBInterface.DiscoverPMs(PMUSBInterface.PMtype.PM3_PRODUCT_NAME2);
            Console.WriteLine("Found {0} PM3_PRODUCT_NAME2...", numErgs);
            numErgs = PMUSBInterface.DiscoverPMs(PMUSBInterface.PMtype.PM4_PRODUCT_NAME);
            Console.WriteLine("Found {0} PM4_PRODUCT_NAME...", numErgs);
            numErgs = PMUSBInterface.DiscoverPMs(PMUSBInterface.PMtype.PM5_PRODUCT_NAME);
            Console.WriteLine("Found {0} PM5_PRODUCT_NAME...", numErgs);

            new Program();
        }

        public Program()
        {
            pm = new PerformanceMonitor(0);

            while (true)
            {
                //pm.HighResolutionUpdate();
                //Console.WriteLine(pm.StrokePhase.ToString());

                pm.LowResolutionUpdate();
                Console.WriteLine("Calories: {0}", pm.Calories);
                //Console.WriteLine("Distance: {0} m", pm.GetHorizontalDistance());
                //Console.WriteLine("TimeWork: {0} sec", pm.GetTimeWork().TotalSeconds);

                //pm.StatusUpdate();
                //Console.WriteLine("Serial: " + pm.Serial);
            }
        }
    }
}